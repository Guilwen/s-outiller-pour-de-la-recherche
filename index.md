---
title: "S'outiller pour de la recherche"
order: 0
in_menu: true
---
Allez, c'est parti ! 

Après presque un an à faire de la recherche, je vous livre mes conseils et logiciels préférés pour une ***recherche dé-GAFAM-iser, libre, et opensource*** !

Je glisserai par-ci par-là ***quelques concepts*** pour illustrer l'intérêt des logiciels.

> Exemple

Je propose ce mini-site en 3 parties :

* Rechercher
* Produire du contenu
* Partager et diffuser votre contenu


## 1/ Rechercher sur internet 

Bon, je passe ici l'importance d'un moteur de recherche et d'un client web respectueux de vos données personnelles (j'utilise personnellement [StartPage](https://www.startpage.com/) avec [Mozilla Firefox](https://www.mozilla.org/), qui sont un très bon combo).

Pour ce qui est propre à la recherche, j'utilise tout le temps Zotero pour gérer mes sources et mes bibliographies. C'est un outil magique.
 
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Zotero.png">
    </div>
    <div>
      <h2>Zotero</h2>
      <p>Zotero permet de collecter, annoter, gérer et citer vos différentes sources, documents électroniques ou papier</p>
      <div>
        <a href="https://framalibre.org/notices/zotero.html">Vers la notice Framalibre</a>
        <a href="https://www.zotero.org/">Vers le site</a>
      </div>
    </div>
  </article>

Pour ce qui est de la veille, on ne fait pas de la veille sur des réseaux... C'est fondamentalement contre-productif : les **algorithmes de recommandation** et le filtrage des posts biaisent vos résultats. Pour éviter ces **bulles d'échos** (*bloqué par ce que l'algorithme nous montre*) et autre biais algorithmique (*confirmation : on voit plus de post en accord avec nos opinions que contre nos opinions, négativité : on voit plus de post à caractère négatif*), il vous suffit de vous rendre sur la page de chaque réseau que vous suivez pour voir s'ils ont fait un nouveau post... 

Oui... Mais pourquoi le faire à la main ? Utilisez un flux RSS : ce n'est pas vous qui allez chercher les nouveaux posts, mais eux qui viennent se ranger dans votre **Agrégateur RSS** (votre gare centrale, avec plein de catégorie, ou les articles viennent se ranger en attendant que vous les lisiez).

Quelques recommandations :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/FreshRSS.png">
    </div>
    <div>
      <h2>FreshRSS</h2>
      <p>FreshRSS permet de s'abonner à des sites pour suivre l'actualité.</p>
      <div>
        <a href="https://framalibre.org/notices/freshrss.html">Vers la notice Framalibre</a>
        <a href="https://freshrss.org">Vers le site</a>
      </div>
    </div>
  </article>

## 2/ Produire du contenu 

Voici d'abord les quelques outils artistiques que j'utilise régulièrement pour créer des schémas, des dessins. Ces outils sont disponibles en application web, mais aussi en application de bureau. Draw.io est plus contraignant, mais son cadrillage permet d'être très minutieux et précis. Excalidraw offre une plus grande facilité d'usage et créativité, mais ses schémas s'accompagnent d'un côté plus "humain", lié à l'absence de grille pour fixer les éléments. 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Excalidraw.png">
    </div>
    <div>
      <h2>Excalidraw</h2>
      <p>Un tableau blanc interactif collaboratif pour dessiner et faire des schémas</p>
      <div>
        <a href="https://framalibre.org/notices/excalidraw.html">Vers la notice Framalibre</a>
        <a href="https://excalidraw.com/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Diagrams.net_Logo.svg/1024px-Diagrams.net_Logo.svg.png">
    </div>
    <div>
      <h2>Draw.io</h2>
      <p>Diagramme simple ou complexe, sécurisé </p>
      <div>
        <a href="https://www.drawio.com/">Vers le site</a>
      </div>
    </div>
  </article>

Concernant l'écriture, abandonnez Word. C'est tellement peu pratique, efficace et intuitif... À la limite, passez à LibreOffice Writer si vous tenez tant à votre interface WYSIWYG (wat u see is wat u get). Sinon, passez en markdown, formez-vous en cinq minutes via les nombreuses ressources en ligne et c'est parti ! 

J'utilise Obsidian pour l'écriture. C'est un compromis. Ce n'est pas Opensource, mais son éthique de protection des données est satisfaisante, et sa communauté élabore de très nombreux add-ons vraiment pratiques.

  <article class="framalibre-notice">
    <div>
      <img src="https://upload.wikimedia.org/wikipedia/commons/1/10/2023_Obsidian_logo.svg">
    </div>
    <div>
      <h2>Obsidian</h2>
      <p>Obsidian est l'application d'écriture privée et flexible qui s'adapte à la manière dont vous pensez. </p>
      <div>
        <a href="https://obsidian.md/">Vers le site</a>
      </div>
    </div>
  </article>

C'est bien sympa Markdown, mais ce n'est pas beau. Oui. C'est un niveau d'écriture assez bas. Ça veut dire que vous pouvez le transformer en n'importe quoi : PDF, présentation, page web, livre. 

Bon, concrètement, voilà deux applications qui permettent, à partir de Markdown, de générer des présentations (MARP) et des PDF (Pandoc). De quoi se débarrasser complétement de Powerpoint et Word.

<article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/MARP.png">
    </div>
    <div>
      <h2>MARP</h2>
      <p>MARP est un logiciel sous licence MIT permetant de faire des présentations en MarkDown</p>
      <div>
        <a href="https://framalibre.org/notices/marp.html">Vers la notice Framalibre</a>
        <a href="https://marp.app">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Pandoc.png">
    </div>
    <div>
      <h2>Pandoc</h2>
      <p>Convertissez toutes sortes de documents avec ce couteau suisse multi-formats.</p>
      <div>
        <a href="https://framalibre.org/notices/pandoc.html">Vers la notice Framalibre</a>
        <a href="http://pandoc.org">Vers le site</a>
      </div>
    </div>
  </article>

## Partager et diffuser 

Et si vous ne voulez pas générer de PDF ou de présentation ? Mais bien rendre accessible vos travaux à tous ? La solution est bien sûr une page web.

Voici le combo gagnant : information stockée sur Gitlab, génération du site par Sphinx et distribution par ReadtheDoc.

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Gitlab.png">
    </div>
    <div>
      <h2>Gitlab</h2>
      <p>Une forge logicielle extrêmement complète&nbsp;: forks, merge requests, tickets, intégration continue, communication avec d'autres services comme Mattermost…</p>
      <div>
        <a href="https://framalibre.org/notices/gitlab.html">Vers la notice Framalibre</a>
        <a href="https://gitlab.com">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Sphinx.png">
    </div>
    <div>
      <h2>Sphinx</h2>
      <p>Sphinx permet de générer très facilement une documentation structurée à partir de fichiers sources.</p>
      <div>
        <a href="https://framalibre.org/notices/sphinx.html">Vers la notice Framalibre</a>
        <a href="http://www.sphinx-doc.org">Vers le site</a>
      </div>
    </div>
  </article>

<article class="framalibre-notice">
    <div>
      <img src="https://upload.wikimedia.org/wikipedia/commons/d/d2/Read-the-docs.png">
    </div>
    <div>
      <h2>ReadTheDocs</h2>
      <p>Read the Docs est une plateforme d'hébergement de documentation de logiciels libres en open-source qui génère de la documentation écrite avec le générateur de documentation Sphinx, MkDocs ou Jupyter Book.</p>
      <div>
        <a href="https://about.readthedocs.com/?ref=readthedocs.com">Vers le site</a>
      </div>
    </div>
  </article> 